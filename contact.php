<?php
    $text = '';

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $errs = array(
            'firstname' => 'No ha insertado un nombre',
            'email' => 'No ha insertado un email',
            'subject' => 'No ha insertado un motivo',
            'invemail' => 'El email introducido no es válido'
        );

        $err = array();
        $checker = false;

        $firstname = isset($_POST['firstname']) && $_POST['firstname'] != '' 
            ? $_POST['firstname']
            : $err[0] = $errs['firstname'];
        $lastname = isset($_POST['lastname']) && $_POST['lastname'] != ''
            ? $_POST['lastname']
            : '';
        $email = isset($_POST['email']) && $_POST['email'] != '' 
            ? $_POST['email']
            : $err[1] = $errs['email'];
        $subject = isset($_POST['subject']) && $_POST['subject'] != '' 
            ? $_POST['subject']
            : $err[2] = $errs['subject'];
        $message = isset($_POST['message']) && $_POST['message'] != ''
            ? $_POST['message']
            : '';

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $err[3] = $errs['invemail'];
        }

        if(!isset($err[0]) && !isset($err[1])
        && !isset($err[2]) && !isset($err[3])){
            $text .= '<div>First name: '.$_POST['firstname'].'</div>';
            $text .= '<div>Last name: '.$_POST['lastname'].'</div>';
            $text .= '<div>Email: '.$_POST['email'].'</div>';
            $text .= '<div>The subject is: '.$_POST['subject'].'</div>';
            $text .= '<div>The message you send is: '.$_POST['message'].'</div>';
        } else {
            $checker = true;
        }
    }

    require "utils/utils.php";
    require "views/contact.view.php";
?>