<?php

class ImagenGaleria
{
    private $nombre;
    private $descripcion;
    private $numVisualizaciones;
    private $numLikes;
    private $numDescargas;
    
    const RUTA_IMAGENES_PORTFOLIO = 'images/index/portfolio/';
    const RUTA_IMAGENES_GALERIA = 'images/index/gallery/';

    public function __construct($nombre, $descripcion, $numVisualizaciones, $numLikes, $numDescargas)
    {
        $this->nombre = $nombre;
        $this->descripcion = $descripcion;
        $this->numVisualizaciones = $numVisualizaciones;
        $this->numLikes = $numLikes;
        $this->numDescargas = $numDescargas;
    }

    public function __toString()
    {
      echo $this->descripcion;   
    }

    public function getNombre()
    {
        return $this->nombre;
    }
    public function setNombre($nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }


    public function getDescripcion()
    {
        return $this->descripcion;
    }
    public function setDescripcion($descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getNumVisualizaciones()
    {
        return $this->numVisualizaciones;
    }
    public function setNumVisualizaciones($numVisualizaciones): self
    {
        $this->numVisualizaciones = $numVisualizaciones;

        return $this;
    }

    public function getNumLikes()
    {
        return $this->numLikes;
    }
    public function setNumLikes($numLikes): self
    {
        $this->numLikes = $numLikes;

        return $this;
    }

    public function getNumDescargas()
    {
        return $this->numDescargas;
    }
    public function setNumDescargas($numDescargas): self
    {
        $this->numDescargas = $numDescargas;

        return $this;
    }

    public function getURLPortfolio() : string 
    {
        return self::RUTA_IMAGENES_PORTFOLIO . $this->getNombre();
    }

    public function getURLGaleria() : string 
    {
        return self::RUTA_IMAGENES_GALERIA . $this->getNombre();
    }
}


/*
** PREGUNTAS:

** ¿Cuál es la diferencia entre private, protected y public?
Los métodos o propiedades privadas no pueden ser llamados por nada que no sea el propio objeto,
los protected pueden ser llamados por objetos heredados pero actuan como private para el resto de cosas
y los public pueden ser llamados en cualquier momento.

** El método __construct es un método mágico en PHP. ¿Qué quiere decir esto?.
Los métodos mágicos son métodos propios de PHP que permiten hacer acciones cuando ocurren los eventos 
a los que llaman, por ejemplo el método __construct permitiría hacer una acción siempre que el objeto 
se construye.

** ¿Una clase en PHP puede tener más de un constructor?
No, PHP no lo permite.

** Vamos a hacer una prueba. Crea una instancia de la clase ImagenGaleria e imprímela con echo. ¿Qué sucede?
*/
// $imagen = new ImagenGaleria('hola', 'pepe', 12, 21, 4);
// echo $imagen;
/*
Muestra un fatal error ya que no se puede hacer echo de un objeto.

** Crea un método mágico __toString() que devuelva la descripción de la imagen a través del getter y prueba ahora a imprimirla de nuevo con echo. ¿Qué ha sucedido?
*/ 
// $imagen->__tostring();
// strval($imagen);
/*

** ¿Cuál es la diferencia entre self y $this?
Self hace referencia a la clase mientras que this hace referencia al objeto invocado.

**
**
*/

?>