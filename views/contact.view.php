<?php
	include __DIR__ . "/partials/inicio-doc.part.php";
	include __DIR__ . "/partials/nav.part.php";
?>

<!-- Principal Content Start -->
<div id="contact">
	<div class="container">
		<div class="col-xs-12 col-sm-8 col-sm-push-2">
			<h1>CONTACT US</h1>
			<hr>
			<p>Aut eaque, laboriosam veritatis, quos non quis ad perspiciatis, totam corporis ea, alias ut unde.</p>
			<form class="form-horizontal" method="POST">
				<div class="form-group">
					<div class="col-xs-6">
						<label class="label-control">First Name</label>
						<span class="bg-danger">
							<?php if (isset($err[0])) echo $err[0] ?>
						</span>
						<input class="form-control" type="text" name="firstname" value="<?php if (isset($_POST['firstname']) && $firstname != $errs['firstname'] && $checker) echo $firstname ?>">
					</div>
					<div class="col-xs-6">
						<label class="label-control">Last Name</label>
						<input class="form-control" type="text" name="lastname" value="<?php if (isset($_POST['lastname']) && $checker) echo $lastname ?>">
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-12">
						<label class="label-control">Email</label>
						<span class="bg-danger">
							<?php
							if (isset($err[1])) {
								echo $err[1];
							} else if (isset($err[3])) {
								echo $err[3];
							}
							?>
						</span>
						<input class="form-control" type="text" name="email" value="<?php if (isset($_POST['email']) && $email != $errs['email'] && $checker) echo $email ?>">
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-12">
						<label class="label-control">Subject</label>
						<span class="bg-danger">
							<?php if (isset($err[2])) echo $err[2] ?>
						</span>
						<input class="form-control" type="text" name="subject" value="<?php if (isset($_POST['subject']) && $subject != $errs['subject'] && $checker) echo $subject ?>">
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-12">
						<label class="label-control">Message</label>
						<textarea class="form-control" name="message"><?php if (isset($_POST['message']) && $checker) echo $message ?></textarea>
						<button class="pull-right btn btn-lg sr-button" name="send">SEND</button>
					</div>
				</div>
			</form>
			<hr class="divider">
			<div class="mt-2">
				<?= $text ?>
			</div>
			<div class="address">
				<h3>GET IN TOUCH</h3>
				<hr>
				<p>Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero.</p>
				<div class="ending text-center">
					<ul class="list-inline social-buttons">
						<li><a href="#"><i class="fa fa-facebook sr-icons"></i></a>
						</li>
						<li><a href="#"><i class="fa fa-twitter sr-icons"></i></a>
						</li>
						<li><a href="#"><i class="fa fa-google-plus sr-icons"></i></a>
						</li>
					</ul>
					<ul class="list-inline contact">
						<li class="footer-number"><i class="fa fa-phone sr-icons"></i> (00228)92229954 </li>
						<li><i class="fa fa-envelope sr-icons"></i> kouvenceslas93@gmail.com</li>
					</ul>
					<p>Photography Fanatic Template &copy; 2017</p>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Principal Content Start -->

<?php require __DIR__ . "/partials/fin-doc.part.php"; ?>