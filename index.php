<?php
    require "entity/ImagenGaleria.php";

    $imagenes = array();

    for ($i=0; $i < 12; $i++) { 
        $imagenes[$i] = new ImagenGaleria($i+1, 'Descripción imagen '.$i+1, rand(0,100), rand(0,100), rand(0,100));
    }

    require "utils/utils.php";
    require "views/index.view.php";
?>